import java.util.Scanner;

public class game1 {
	static Scanner kb;
	static char[][] Board;
	static int i, Winner, Player, Go, Row, Column, Line;

	public static void main(String[] args) {
		kb = new Scanner(System.in);
		i = 0 ;
		Winner = 0 ;
		Player = 0 ;
		Go = 0 ;
		Row = 0 ;
		Column = 0 ;
		Line = 0 ;
		Board = new char [][]{
			{'1','2','3'},
			{'4','5','6'},
			{'7','8','9'}
		} ;	
		
		showWelcome() ;
		showBoard() ;
		input() ;
		showWinner() ;				
			}

	static void showWelcome() {
		System.out.println("\t\t\t " + "     Welcome to Game XO" + "\n") ;
	}

	static void showBoard() {
			System.out.printf("\t\t\t\t %c | %c | %c\n" , Board[0][0] , Board[0][1] , Board[0][2]) ;
			System.out.print ("\t\t\t\t---+---+---\n") ;
			System.out.printf("\t\t\t\t %c | %c | %c\n" , Board[1][0] , Board[1][1] , Board[1][2]) ;
			System.out.print ("\t\t\t\t---+---+---\n") ;
			System.out.printf("\t\t\t\t %c | %c | %c\n" , Board[2][0] , Board[2][1] , Board[2][2]) ;

	}
	static void input()	{
		for(i = 0 ; i < 9 && Winner == 0 ; i++) {
			switchPlayer() ;
 			do {
				System.out.println("") ;
				System.out.printf("\n\t\t Player %d, please enter the number [your] %c : " , Player , (Player == 1)? 'X' : 'O') ;
				
				Go = kb.nextInt() ;
				if(!(Go >=1 && Go <= 9)) {
					System.out.print("\t\t\t\t!!! ERROR !!!") ;
				}
				getRowColumn() ;
			}
			while(Go < 0 || Go > 9 || Board[Row][Column] > '9') ;
			Board[Row][Column] = (Player == 1) ? 'X' :'O' ;
			checkWinner() ;
		}
	}
	static void getRowColumn() {
		Row = --Go / 3 ;
		Column = Go % 3 ;
	}
	static void switchPlayer() {
		if(Player == 0 || Player == 1) {
			Player +=1 ;
		}
		else { 
			Player -=1 ;
		}
	}
	static void checkWinner() {
		if((Board[0][0] == Board[1][1] && Board[0][0] == Board[2][2]) ||
				(Board[0][2] == Board[1][1] && Board[0][2] == Board[2][0])) {
			Winner = Player ;
		}
		else {
			for(Line = 0 ; Line <= 2 ; Line++) {
				if((Board[Line][0] == Board[Line][1] && Board[Line][0] == Board[Line][2])||
						(Board[0][Line] == Board[1][Line] && Board[0][Line] == Board[2][Line])) {
					Winner = Player ;
				}
					
			}
		}
		showBoard() ;
	}
	static void showWinner() {
		if(Winner==0) {
			System.out.printf("\n\t\t\tHow boring, it is a draw.\n") ;
		}
		else {
			System.out.printf("\n\t\t\"Congratuiations\", Player %d , YOU ARE THE WINNER!\n", Winner);
		}
	}
}
