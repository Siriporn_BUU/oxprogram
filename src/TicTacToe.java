import java.util.* ;
	public class TicTacToe {
		static Scanner kb ;
		static String[] Board ;
		static String Turn , Winner , w ;
	 
		public static void main(String[] args) {
			kb = new Scanner (System.in) ;
			Board = new String [9] ;
			Turn = "X" ;
			
			showWelcome() ;
			printBoard() ;
			input() ;
			
			
			if (Winner.equalsIgnoreCase("draw")) {
				System.out.println("It's a draw! Thanks for playing.");
			} 
			else {
				System.out.println("Congratulations! Mr." + Winner + "'s you're WINNER! Thanks for playing.");
			}

		}
		static void showWelcome() {
			System.out.println("\t\t\t\t\t " + "     Welcome to Game XO" + "\n") ;
		}
		static void input() {
			while (Winner == null) {
				System.out.println("Mr."+ Turn +  "'s. Please enter a slot number[1 to 9] to place " + Turn + " in : ") ;
				int N ;
				try {
					N = kb.nextInt() ;
					if(!(N > 0 && N <= 9)) {
						System.out.println("Invalid input.... Pls. re-enter slot number: ") ;
						continue ;
					}
				} 
				catch (InputMismatchException e) {
					System.out.println("Invalid input.... Pls. re-enter slot number: ") ;
					continue ;
				}
				if (Board[N - 1].equals(String.valueOf(N))) {
					Board[N - 1] = Turn ;
					if(Turn.equals("X")) {
						Turn = "O" ;
					}
					else
						Turn = "X" ;
					printBoard() ;
					checkWin() ;
				}
				else {
					System.out.println("Slot already taken.... Pls. re-enter slot number : ") ;
					continue ;
				}
			}
		}
 		static String checkWin() {
			for(int a = 0 ; a < 3 ; a++) {
				if((Board[0] == Board[4] && Board[0] == Board[8]) || (Board[6] == Board[4] && Board[6] == Board[2])) {
					Winner = Board[4] ;
				}
				else if(Board[a].equals(Board[a + 3]) && Board[a + 3].equals(Board[a + 6]) ) {
					Winner = Board[a] ;
				}
				else if(Board[0] == Board[1] && Board[1] == Board[2] || Board[3] == Board[4] && Board[4] == Board[5] || Board[6] == Board[7] && Board[7] == Board[8]) {
					Winner = Board[a] ;
				}
				else {
					break ;
				}
			}
			for (int a = 0; a < 9 ; a++) {
				if (Arrays.asList(Board).contains(String.valueOf(a + 1))) {
					break;
				}
				else if (a == 8) 
					return "draw";
			}
			return Winner ;
		}
		static void printBoard() {
			for (int a = 0 ; a < 9 ; a++) {
				Board[a] = String.valueOf(a + 1) ;
			}
			System.out.println("\t\t\t\t\t\t " + Board[0] + " | " + Board[1] + " | " + Board[2]);
			System.out.println("\t\t\t\t\t\t" + "-----------");
			System.out.println("\t\t\t\t\t\t " + Board[3] + " | " + Board[4] + " | " + Board[5]);
			System.out.println("\t\t\t\t\t\t" + "-----------");
			System.out.println("\t\t\t\t\t\t " + Board[6] + " | " + Board[7] + " | " + Board[8]);
		}

	}


